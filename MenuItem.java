package com.kyerussell.menu;

/**
 * An item to be used in a {@link Menu}.
 *
 * This is used as a hack around Java's lack of real function/method pointers.
 * You should inherit from MenuItem, fillling the {@link #run()} method with the
 * code you want to run when this option is selected.
 *
 * This is easy to do anonymously, for example, if you had a {@link Menu} called <code>menu</code>
 * <pre>
 * {@code
 * Menu menu = new Menu("My Cool Menu");
 * menu.add("Exit", new MenuOption() { run() { System.exit(0); }});
 * }
 * </pre>
 */
public interface MenuItem {
	void run();
}
