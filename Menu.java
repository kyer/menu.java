package com.kyerussell.menu;

import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.kyerussell.console.Console;

public class Menu {
	private String name;
	private final String prompt = ">>> ";
	private LinkedHashMap<String, MenuItem> items = new LinkedHashMap<String, MenuItem>();

	/**
	 * Create a new menu.
	 * 
	 * A menu consists of a list of options, each one correlating to
	 * a MenuOption, which is essentially a wrapper for your code.
	 * 
	 * @param name The name of the menu, this'll be displayed before the choice list.
	 */
	public Menu(String name)
	{
		if ((name == null) || (name.equals("")))
		{
			throw new IllegalArgumentException("menu name must not be empty or null");
		}
		
		this.name = name;
	}
	
	/**
	 * Create a new menu.
	 * 
	 * A menu consists of a list of options, each one correlating to
	 * a MenuOption, which is essentially a wrapper for your code.
	 * 
	 * @param name The name of the menu, this'll be displayed before the choice list.
	 * @param prompt the 'prompt' text to display when asking for user's input, e.g. "Enter a choice: "
	 */
	public Menu (String name, String prompt)
	{
		this(name);
		
		if ((prompt == null) || (prompt.equals("")))
		{
			throw new IllegalArgumentException("menu prompt must not be empty or null");
		}
		
		this.name = name;
	}

	/**
	 * Add a choice to the menu.
	 * 
	 * @param name The name of the choice. This is what's displayed in the choice list, and what the
	 *             user will have to type in to select the choice. 
	 * @param item The MenuItem correlating to the choice. See MenuItem for more information.
	 */
	public void add(String name, MenuItem item)
	{
		items.put(name, item);
	}
	
	/**
	 * 'Execute' the menu.
	 * 
	 * Display the choice list, and have the user make a selection. Invalid selections are
	 * caught automatically, and the prompt is re-displayed, so the user has to make a valid
	 * selection.
	 */
	public void go()
	{
		// attribute: http://stackoverflow.com/questions/2351331/iterating-hashtable-in-javahttp://stackoverflow.com/questions/2351331/iterating-hashtable-in-java
		Iterator<Entry<String, MenuItem>> it = items.entrySet().iterator();

		
		// first, print menu options and prompt for a response.
		System.out.printf("\n%s\n\n", this.name);
		
		while (it.hasNext()) {
			Entry<String, MenuItem> entry = it.next();
			System.out.printf("- %s\r\n", entry.getKey());
		}
		
		boolean complete = false; // by default, the user hasn't selected a correct menu option.

		// repeat until the user correctly selects a menu option.
		while (!complete)
		{
			String response = Console.readLine(this.prompt);
	
			// once menu is responded to, find the menu element the user selected
			// and execute its callback function. 
			it = items.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, MenuItem> entry = it.next();
				if(entry.getKey().toLowerCase().equals(response.toLowerCase())) // case-insensitive match.
				{
					complete = true;
					entry.getValue().run();
				}
			}
			
			if (!complete)
			{
				System.out.println("Incorrect selection. Type one of the commands above.");
			}
		}
	}
}
